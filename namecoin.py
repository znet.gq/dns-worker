# NAMECOIN_RPC_URL = "http://127.0.0.1:8336/"
# NAMECOIN_RPC_COOKIEPATH = "/home/ivanq/.namecoin/.cookie"

NAMECOIN_RPC_URL = None
NAMECOIN_RPC_COOKIEPATH = None


import subprocess
import requests
import datetime
import json
import re
import os


session = requests.Session()


def rpc_query(cmd, *args):
    with open(NAMECOIN_RPC_COOKIEPATH) as f:
        rpc_auth = tuple(f.read().strip().split(":"))
    return session.post(NAMECOIN_RPC_URL, data=json.dumps({
        "jsonrpc": "1.0",
        "method": cmd,
        "params": args
    }), auth=rpc_auth).json()["result"]


def load_block_info(block_hash):
    if NAMECOIN_RPC_URL:
        data = rpc_query("getblock", block_hash)
        return {
            "hash": block_hash,
            "height": data["height"],
            "date": datetime.datetime.fromtimestamp(data["time"]).strftime("%b. %d, %Y, %-H:%M %P").replace("am", "a.m.").replace("pm", "p.m."),
            "previous_hash": data.get("previousblockhash"),
            "next_hash": data.get("nextblockhash"),
            "transactions": data["tx"]
        }
    else:
        lines = [line for line in [line.strip().replace("<td>", "").replace("</td>", "") for line in session.get(f"https://namebrow.se/block/{block_hash}/").text.split("\n")] if line]
        return {
            "hash": block_hash,
            "height": int(lines[lines.index("Height") + 1]),
            "date": lines[lines.index("Date/time") + 1],
            "previous_hash": lines[lines.index("Previous block") + 1].split(">")[1].split("<")[0] or None,
            "next_hash": lines[lines.index("Next block") + 1].split(">")[1].split("<")[0].replace("None", "") or None,
            "transactions": [line.split(">")[2].split("<")[0] for line in lines if line.startswith("<th colspan=\"3\">Transaction")]
        }


def load_tx_info(tx_hash):
    if NAMECOIN_RPC_URL:
        rawhex = rpc_query("getrawtransaction", tx_hash)
        data = rpc_query("decoderawtransaction", rawhex)
        return {
            "transaction_outputs": [
                {"script_asm": vout["scriptPubKey"]["asm"]}
                for vout in data["vout"]
            ]
        }
    else:
        return session.get(f"https://namebrow.se/api/tx/{tx_hash}/").json()


def add_domain(domain, value, block_info):
    try:
        with open("domains/" + domain + ".json") as f:
            data = f.read()
    except FileNotFoundError:
        data = "{}"
    data = json.loads(data)

    if "history" not in data:
        data["history"] = []
    data["history"].append({
        "address": value,
        "date": block_info["date"],
        "source": "namecoin",
        "source_info": {
            "block_hash": block_info["hash"]
        }
    })

    with open("domains/" + domain + ".json", "w") as f:
        f.write(json.dumps(data, indent=4))


def handle_op_name_update(bytes_name, bytes_value, block_info):
    try:
        name = bytes_name.decode()
        value = json.loads(bytes_value.decode())
    except UnicodeDecodeError:
        return
    except json.JSONDecodeError:
        return

    if not name.startswith("d/") or not re.match(r"^d/[A-Za-z0-9_-]+$", name) or not isinstance(value, dict):
        return

    domain = name[2:].lower() + ".bit"

    if "zeronet" in value:
        domains = value["zeronet"]
        if isinstance(domains, str):
            domains = {"": domains}
        if isinstance(domains, dict):
            for subdomain, address in domains.items():
                subdomain = subdomain.lower()
                if not isinstance(address, str):
                    continue
                if subdomain == "*" or subdomain == "":
                    subdomain = domain
                elif all(re.match(r"^[A-Za-z0-9_-]+$", part) for part in subdomain.split(".")):
                    subdomain = f"{subdomain}.{domain}"
                else:
                    continue
                add_domain(subdomain, address, block_info)

    if "map" in value and isinstance(value["map"], dict):
        for subdomain, data in value["map"].items():
            subdomain = subdomain.lower()
            if subdomain == "*" or subdomain == "":
                subdomain = domain
            elif all(re.match(r"^[A-Za-z0-9_-]+$", part) for part in subdomain.split(".")):
                subdomain = f"{subdomain}.{domain}"
            else:
                continue

            if "zeronet" in data and isinstance(data["zeronet"], str):
                address = data["zeronet"]
                add_domain(subdomain, address, block_info)

        if "zeronet" in value["map"] and isinstance(value["map"]["zeronet"], str):
            address = value["map"]["zeronet"]
            add_domain(domain, address, block_info)


os.chdir("../dns")


with open("info/namecoin.json") as f:
    last_mirrored_block = json.loads(f.read())["last_mirrored_block"]


def save(height):
    subprocess.call(["git", "add", "."])
    subprocess.call(["git", "commit", "-m", f"Height {height}"])
    subprocess.call(["git", "push", "origin-ci", "master"])


while True:
    info = load_block_info(last_mirrored_block)
    if not info["next_hash"]:
        break

    print(info["height"], info["date"], f"({len(info['transactions'])})", flush=True)
    for txid in info["transactions"]:
        tx = load_tx_info(txid)
        for item in tx["transaction_outputs"]:
            words = item["script_asm"].split(" ")
            for i, word in enumerate(words):
                if word == "OP_NAME_UPDATE":
                    try:
                        bytes_name = bytes.fromhex(words[i + 1])
                        bytes_value = bytes.fromhex(words[i + 2])
                    except ValueError:  # non-hex value is perfectly valid outside d/
                        pass
                    else:
                        handle_op_name_update(bytes_name, bytes_value, info)

    last_mirrored_block = info["next_hash"]

    with open("info/namecoin.json", "w") as f:
        f.write(json.dumps({"last_mirrored_block": last_mirrored_block}))


    if info["height"] % 20000 == 0:
        save(info["height"])


save(info["height"])
